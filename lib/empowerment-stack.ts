import * as path from 'path';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as rds from 'aws-cdk-lib/aws-rds';
import * as secretsmanager from 'aws-cdk-lib/aws-secretsmanager';
import * as cdk from 'aws-cdk-lib';
import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

const projectName = 'empowerment-infra';
const { STAGE = 'dev' } = process.env;

export class EmpowermentInfraStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, `${projectName}-${STAGE}-cdk-vpc`, {
      subnetConfiguration: [
        {
          name: 'Isolated',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
        },
        {
          name: 'Private',
          subnetType: ec2.SubnetType.PRIVATE_WITH_NAT,
        },
        {
          name: 'Public',
          subnetType: ec2.SubnetType.PUBLIC,
        }
      ]
    });

    const dbUsername = 'postgres';
    const rdsSecret = new secretsmanager.Secret(this, `${projectName}-${STAGE}-cdk-secrets`, {
      secretName: id+'-rds-credentials',
      generateSecretString: {
        secretStringTemplate: JSON.stringify({ username: dbUsername }),
        generateStringKey: 'password',
        excludePunctuation: true,
        includeSpace: false,
      }
    });

    // Create a security group to be used on the lambda functions
    const lambdaSecurityGroup = new ec2.SecurityGroup(this, `${projectName}-${STAGE}-lambda-sg`, {
      vpc
    });

    // Create a security group to be used on the RDS proxy
    const rdsProxySecurityGroup = new ec2.SecurityGroup(this, `${projectName}-${STAGE}-only-access-from-lambda`, {
      vpc
    });
    rdsProxySecurityGroup.addIngressRule(lambdaSecurityGroup, ec2.Port.tcp(5432), 'allow lambda connection to rds proxy');

    // Create a security group to be used on the RDS instances
    const rdsSecurityGroup = new ec2.SecurityGroup(this, `${projectName}-${STAGE}-only-access-from-rds-proxy`, {
      vpc
    });
    rdsSecurityGroup.addIngressRule(rdsProxySecurityGroup, ec2.Port.tcp(5432), 'allow db connections from the rds proxy');
    
    const rdsCredentials = rds.Credentials.fromSecret(rdsSecret);

    const dbName = 'nflstadiums';
    const postgreSql = new rds.DatabaseCluster(this, `${projectName}-${STAGE}-cdk-rds-proxy`, {
      instanceProps:
      {
        vpc,
        vpcSubnets: {
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED
        },
        instanceType: ec2.InstanceType.of(ec2.InstanceClass.T3, ec2.InstanceSize.MEDIUM),
        securityGroups: [ rdsSecurityGroup ]
      },
      clusterIdentifier: 'RDSProxyEmpowerCluster',
      engine: rds.DatabaseClusterEngine.auroraPostgres({
        version: rds.AuroraPostgresEngineVersion.VER_11_9
      }),
      instances: 1,
      backup: { retention: cdk.Duration.days(1) },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      credentials: rdsCredentials,
      defaultDatabaseName: dbName,
    });

    const rdsProxy = postgreSql.addProxy(`${projectName}-${STAGE}-cdk-rds-proxy`, {
      secrets: [ rdsSecret ],
      securityGroups: [ rdsProxySecurityGroup ],
      debugLogging: true,
      iamAuth: true,
      vpc
    });

    const lambdaFunction = new cdk.aws_lambda_nodejs.NodejsFunction(this, `${projectName}-${STAGE}-lambda-function`, {
      entry: path.join(__dirname, '../app/src/functions/hello', 'lambda.ts'),
      runtime: cdk.aws_lambda.Runtime.NODEJS_16_X,
      timeout: cdk.Duration.minutes(3), // Default is 3 seconds
      memorySize: 1024,
      environment: {
        PGHOST: rdsProxy.endpoint,
        PGDATABASE: dbName,
        PGUSER: dbUsername,
        STAGE: "development"
      },
      bundling: {
        externalModules: [
          'aws-sdk', // Use the 'aws-sdk' available in the Lambda runtime
          'pg-native',
          'pg-hstore'
        ],
      },
      vpc,
      vpcSubnets: vpc.selectSubnets({
        subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
      }),
      securityGroups: [lambdaSecurityGroup],
    });

    

    rdsProxy.grantConnect(lambdaFunction, dbUsername);
    const fnUrl = lambdaFunction.addFunctionUrl({
      authType: cdk.aws_lambda.FunctionUrlAuthType.NONE,
    })


    new cdk.CfnOutput(this, `${projectName}-${STAGE}-dbProxy`, {
      value: rdsProxy.endpoint,
    });

    new cdk.CfnOutput(this, `${projectName}-${STAGE}-lambda`, {
      value: fnUrl.url,
    });

    

    new cdk.CfnOutput(this, `${projectName}-${STAGE}-dbEndpoint`, {
      value: postgreSql.clusterEndpoint.hostname,
    });

    new cdk.CfnOutput(this, `${projectName}-${STAGE}-secretName`, {
      value: postgreSql.secret?.secretName!,
    });

  }
}
