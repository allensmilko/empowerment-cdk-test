import {sequelize} from '../libs/sequelize';
import { DataTypes } from 'sequelize';

const Stadium = sequelize.define('Stadium', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  capacity: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  location: {
    type: DataTypes.STRING,
    allowNull: false
  },
  surface: {
    type: DataTypes.STRING,
    allowNull: false
  },
  roof: {
    type: DataTypes.STRING,
    allowNull: false
  },
  team: {
    type: DataTypes.STRING,
    allowNull: false
  },
  yearOpened: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  tableName: 'stadiums'
});

(async () => {
  await sequelize.sync({ force: true });
  // Code here
})();

export { Stadium, sequelize };