import * as pg from 'pg';
import { Sequelize, Options as SequelizeOptions } from 'sequelize';
import { Signer } from 'aws-sdk-js-v3-rds-signer';

const stage = process.env.STAGE || 'local';
const sequelizeConfig: SequelizeOptions = {
  host: process.env.PGHOST || 'nflstadiums.cx6ktwqkieho.us-east-1.rds.amazonaws.com',
  dialectModule: pg,
  database: 'postgres',
  dialect: 'postgres'
};

if (stage !== 'local') {
  sequelizeConfig.dialectOptions = {
    ssl: {
      rejectUnauthorized: true
    }
  };
}

const signer = new Signer({
  hostname: process.env.PGHOST,
  port: 5432,
  region: process.env.AWS_REGION || 'us-east-1',
  username: 'postgres'
});

const sequelize = new Sequelize(process.env.PGDATABASE || 'postgres', process.env.PGUSER || 'postgres', process.env.PGPASSWORD || 'postgres', sequelizeConfig);

if (stage !== 'local' && !sequelize.hasHook('beforeConnect')) {
  sequelize.addHook('beforeConnect', async (config) => {
    // @ts-ignore
    config.password = await signer.getAuthToken();
  });
}

export { sequelize };